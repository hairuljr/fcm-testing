// Add Firebase products that you want to use
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js');

// Firebase SDK
firebase.initializeApp({
  apiKey: "AIzaSyDWn7L_zzaqhXaGBDWCDxklnBvPdiWm89U",
  authDomain: "i-tap-ca92b.firebaseapp.com",
  projectId: "i-tap-ca92b",
  storageBucket: "i-tap-ca92b.appspot.com",
  messagingSenderId: "1013189263054",
  appId: "1:1013189263054:web:40401d9dd527065b5d5a9f",
  measurementId: "G-PHED85VCFQ"
});

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  console.log("Message has received : ", payload);
  const title = "First, solve the problem.";
  const options = {
    body: "Push notificaiton!",
    icon: "/chat.png",
  };
  return self.registration.showNotification(
    title,
    options,
  );
});