<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/send-web-push-notificaiton', [App\Http\Controllers\UserController::class, 'index'])->name('send-push.notificaiton');

Route::post('/save-device-token', [App\Http\Controllers\UserController::class, 'saveDeviceToken'])->name('save-device.token');
Route::post('/send-notification', [App\Http\Controllers\UserController::class, 'sendNotification'])->name('send.notification');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
