<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function saveDeviceToken(Request $request)
    {
        auth()->user()->update(['device_token' => $request->token]);
        return response()->json(['Token stored.']);
    }

    public function sendNotification(Request $request)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        // $DeviceToken = User::whereNotNull('device_token')->pluck('device_token')->all();
        $token = User::whereEmail(auth()->user()->email)->whereNotNull('device_token')->select('device_token')->first()->device_token;

        $FcmKey = 'AAAA6-bJPs4:APA91bHHU6qzgn34LtHh-rBkpu5DELvYYwZIZAN8_Luegt7r2v4kxXfDLax0AbmYRWo1bD9c8jMx3H6YGt6SkXaWK1Fp_PBUiish9Z7CtlOpu1cua5p_lV8TWWWjG5BrtLxJ16kOUeXy';

        $data = [
            "to" => $token,
            "notification" => [
                "title" => $request->title,
                "body" => $request->body,
                "icon" => asset('chat.png'),
                "click_action" => "http://localhost:8000"
            ]
        ];

        $RESPONSE = json_encode($data);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Authorization: key=AAAA6-bJPs4:APA91bHHU6qzgn34LtHh-rBkpu5DELvYYwZIZAN8_Luegt7r2v4kxXfDLax0AbmYRWo1bD9c8jMx3H6YGt6SkXaWK1Fp_PBUiish9Z7CtlOpu1cua5p_lV8TWWWjG5BrtLxJ16kOUeXy",
            "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $RESPONSE);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        return back();
    }
}
